<?php

class msg {
	public $msgid;
	public $titre;
	public $contenu;
	public $ip;
    public $date;
    public $nblikemsg;
    public $nbdislikemsg;
	
	function __construct() {
		$this->msgid = 0;
		$this->titre = "";
		$this->contenu = "",
        $this->ip = "";
        $this->date = new DateTime();
        $this->nblikemsg = 0;
        $this->nbdislikemsg = 0;
	}
	
	public static function construit_params($msgid, $titre, $contenu, $ip, $date, $nblikemsg, $nbdislikemsg) {
		$msg = new msg();
		
		$msg->msgid = $msgid;
		$msg->titre = $titre;
		$msg->contenu = $contenu;
		$msg->ip = $ip;
        $msg->date = new DateTime($date);
        $msg->nblikemsg = $nblikemsg;
        $msg->nbdislikemsg = $nbdislikemsg;
		
		return $msg;
	}
	
	public static function loadUnMsg($msgid) {
		$msg = new msg();
		
		//sql
		$requete = "select titre, contenu, ip, date, nblikemsg, nbdislikemsg FROM msg WHERE msgid=?";
		$stmt = $GLOBALS['lien_bdd']->prepare($requete);
		$stmt->bind_param("i", $msgid);
		$stmt->execute();
		$stmt->bind_result($titre, $contenu, $ip, $date, $nblikemsg, $nbdislikemsg);
		$stmt->fetch();
		$msg->msgid = $msgid;
		$msg->titre = $titre;
		$msg->contenu = $contenu;
		$msg->ip = $ip;
        $msg->date = new DateTime($date);
        $msg->nblikemsg = $nblikemsg;
        $msg->nbdislikemsg = $nbdislikemsg;
		
		return $msg;
	}
	
}