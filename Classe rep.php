<?php

class rep {
	public $ReponseID;
	public $contenu;
	public $msgid;
	public $datereponse;
    public $nblikerep;
    public $nbdislikerep;
    public $ip;
	
	function __construct() {
		$this->ReponseID = 0;
		$this->contenu = "";
		$this->msgid = 0,
        $this->datereponse = new DateTime();
        $this->nblikerep = 0;
        $this->nbdislikerep = 0;
        $this->ip = "";
	}
	
	public static function construit_params($ReponseID, $contenu, $msgid, $datereponse, $nblikerep, $nbdislikerep, $ip) {
		$rep = new rep();
		
		$rep->ReponseID = $ReponseID;
		$rep->contenu = $contenu;
		$rep->msgid = $msgid;
		$rep->datereponse = new DateTime($datereponse);
        $rep->nblikerep = $nblikerep;
        $rep->nbdislikerep = $nbdislikerep;
        $rep->ip = $ip;
		
		return $rep;
	}
	
	public static function loadUnerep($ReponseID) {
		$rep = new rep();
		
		//sql
		$requete = "select Contenu, msgid, Datereponse, nblikerep, nbdislikerep, IP from reponse where ReponseID=?";
		$stmt = $GLOBALS['lien_bdd']->prepare($requete);
		$stmt->bind_param("i", $ReponseID);
		$stmt->execute();
		$stmt->bind_result($contenu, $msgid, $datereponse, $nblikerep, $nbdislikerep, $ip);
		$stmt->fetch();
		$msg->ReponseID = $ReponseID;
		$msg->contenu = $contenu;
		$msg->msgid = $msgid;
		$msg->datereponse = new DateTime($datereponse);
        $msg->nblikerep = $nblikerep;
        $msg->nbdislikerep = $nbdislikerep;
        $msg->ip = $ip;
		
		return $rep;
	}
	
}